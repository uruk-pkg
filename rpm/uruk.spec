# Maintained using git at http://git.mdcc.cx/uruk-pkg

# Copyright (C) 2004 - 2005, 2008, 2011 - 2015, 2017 - 2018
#  Tilburg University http://www.uvt.nl/
#
# This file is part of uruk.  Uruk is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.  You should have received a copy of
# the GNU General Public License along with this file (see COPYING).

%define version 20181114
%define release 1

%define name uruk
%define source_ext gz

%define uruksbindir /sbin
%define uruklibexecdir /lib

Summary:   Very small firewall script, for configuring iptables
Name:      %{name}
Version:   %{version}
Release:   %{release}

BuildArch: noarch
Source0:   %{name}-%{version}.tar.%{source_ext}
Source1:   %{name}-source.1.README.RPM
Source2:   %{name}-source.2.TODO.RPM

# Patch0:    %{name}-patch.0.init-chkconfig

# Requires : redhat-lsb-core  : works on Fedora >= 12, not RHEL.
# RHEL release 5.8 is known to need redhat-lsb
# RHEL 6 initscripts-9.03.31-2.el6.x86_64 ships /etc/init.d/functions
Requires : initscripts, iptables

License:   GPL
Group:     Applications/System
Vendor:    Joost van Baal-Ilić
URL:       http://mdcc.cx/uruk/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
Prefix:    %{_prefix}

%description
 Uruk is a wrapper for Linux ip[6]tables.  A very simple shell script, but
 useful if you need similar (but not the same) packet filtering configurations
 on lots of hosts.  It uses a template file, which gets sourced as a shell
 script, to get lists of source addresses, allowed to use specific network
 services.  Listing these groups of allowed hosts and allowed services is all
 what's needed to configure your box.
 Main difference with other firewall setup tools: uruk is just a very small
 (just 11K!) shell script, no gui, no interactive setup, no default
 configuration.  You'll like this if you'd rather not have lots of (probably
 buggy) code between you and your filtering rules.

%prep
%setup -q
# %patch0 -p1
cp %{_sourcedir}/%{name}-source.1.README.RPM README.RPM
cp %{_sourcedir}/%{name}-source.2.TODO.RPM TODO.RPM

%build
%configure --libexecdir=%{uruklibexecdir} --sbindir=%{uruksbindir}
make

%install
rm -fr %{buildroot}
make DESTDIR=%{buildroot} install
rm -rf %{buildroot}/usr/share/doc/uruk
mkdir -p %{buildroot}/etc/rc.d/init.d
mv %{buildroot}/etc/init.d/uruk %{buildroot}/etc/rc.d/init.d/
rmdir %{buildroot}/etc/init.d
mkdir -p %{buildroot}/etc/sysconfig
cp %{_builddir}/%{name}-%{version}/doc/default %{buildroot}/etc/sysconfig/uruk

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add uruk
/sbin/service uruk force-reload

%preun
if test "$1" = 0
then
    /sbin/chkconfig --del uruk
fi

%files
%defattr(-,root,root)
%doc AUTHORS NEWS README THANKS TODO ChangeLog ChangeLog.2003 doc/rc man/*.html README.RPM TODO.RPM
%config /etc/rc.d/init.d/uruk
%config /etc/sysconfig/uruk
%{uruksbindir}/uruk
%{uruksbindir}/uruk-save
%{uruksbindir}/urukctl
%{uruklibexecdir}/uruk
/lib/systemd/system/uruk.service
%{_mandir}/man5
%{_mandir}/man8

%changelog
* Mon Nov 13 2018  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20181114-1
- New upstream prerelease.

* Mon Nov 13 2018  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20181113-1
- New upstream: The Alfama Release.

* Mon Nov 12 2018  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20181112-1
- New upstream: The Plantagedok Release.

* Wed Nov 9 2018  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20181109-1
- New upstream: The Catharijnepoort Release.

* Wed Nov 7 2018  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20181107-1
- New upstream: The Daničareva Release.  (Missed:
   20181006 - The Tidal Basin, Washington, D.C. Release
   20181005 - The Dampad Release
   20180528 - The Verliefd Laantje Release
  ).
- This release drops support for RHEL < 6.

* Fri Jul 21 2017  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20160219-1
- New upstream: The Speurgt Release.  (Missed:
   20160218 - The Snijders-Chaam Release
  ).

* Wed Dec 9 2015  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20151118-2
- Add 'service uruk force-reload' to %post: restart on upgrades.
- Rebuild on RHEL AS 4 Nahant Update 9

* Wed Dec 9 2015  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20151118-1
- New upstream: The Āne-wātak Release.  (Missed:
   20150921 - The Prishtinë Release
   20150916 - The ᎠᏍᎦᏯ ᎩᎦᎨᏱ; Release
   20150825 - The Прибој Release
   20150810 - The Гoрњи Милановац Release
  ).
- Register experimental file /lib/systemd/system/uruk.service in package.
  Will have no impact on non-systemd Red Hat systems (like <= RHEL6).
- No longer install useless /u/s/d/u/man/{Makefile,include}*,
  man/{uruk-rc,uruk-save,uruk,urukctl}.{5,8,azm,ps,txt}; just install
  man/{uruk-rc,uruk-save,uruk,urukctl}.html.
- Install /u/s/d/u/ChangeLog.2003.

* Mon Jun 8 2015  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20150608-2
- Rebuild on RHEL AS 4 Nahant Update 9

* Mon Jun 8 2015  Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20150608-1
- New upstream: The Oude Leije Release. (Missed:
   20150401 - The Gorp en Roovert Release
   20150325 - The De Drie Zwaantjes Release
  ).
- Update copyright.

* Fri Nov 28 2014 Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20141120-1
- New upstream: The Јадар Release.

* Fri Oct 24 2014 Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20140627-1
- New upstream: The Vlook Release.

* Sun Apr 6 2014 Joost van Baal-Ilić <joostvb-uruk@mdcc.cx> 20140319-1
- This RPM was never released.
- New upstream: The Alfama Release.  (Missed: 20131213 - The Gweek Release).

* Sun Sep 15 2013 Joost van Baal-Ilić <joostvb@uvt.nl> 20130913-1
- New upstream: The Clochán na bhFomhórach Release.

* Fri Aug 30 2013 Joost van Baal-Ilić <joostvb@uvt.nl> 20130830-1
- New upstream: The Dr Syntax's Head Release.  (Missed:
   20130809 - The Corbeşti Release
   20130619 - The Het De Siptenpad Release
   20130618 - The Sterreke Release
   20130426 - The Sy Release
  ).
- Ship new urukctl script and urukctl(8) manpage (and get rid of hardcoded list
  of manpages in .spec (we now ship .azm, and .5 and .8 twice)).
- Install doc/default as config file /etc/sysconfig/uruk.  Thanks Casper Gielen
  for suggestion.

* Thu Feb 27 2013 Joost van Baal-Ilić <joostvb@uvt.nl> 20130226-1
- New upstream: The Vlist Release.

* Thu Feb 14 2013 Joost van Baal-Ilić <joostvb@uvt.nl> 20121205-1
- New upstream: The Zes Blokskes Release.  (Missed: 20121130 - The Вршац
  Release).
- Updated %files: installs in /lib/uruk/init/*, next to /lib/uruk/lsb/*.

* Fri Nov 2 2012 Joost van Baal-Ilić <joostvb@uvt.nl> 20121023-1
- New upstream: The Grafwegen Release.  This release fixes a grave bug in
  dealing with ICMPv6 Local Configuration Traffic; IPv6 firewalling was
  unusable.

* Fri Oct 5 2012 Joost van Baal-Ilić <joostvb@uvt.nl> 20121005-1
- New upstream: The Onze-Lieve-Vrouw-Waver Release.  (Missed: 20120914 -
  The Sankt Goar Release.)
- No longer Requires redhat-lsb, but just initscripts: upstream now implements
  needed LSB shell functions.
- Add --libexecdir=/lib --sbindir=/sbin to configure call, in order to support
  new upstream init script.
- Updated %files: script in /sbin (no longer /usr/sbin), new lsb shell functions
  in /lib/uruk/lsb/.
- Vendor: update my name.

* Tue Jul 17 2012 Joost van Baal-Ilić <joostvb@uvt.nl> 20120608-1
- New upstream.  (Missed: 20120530, 20120605.)
- No longer build-depends upon zoem.
- uruk-source.1.README.RPM, uruk-source.2.TODO.RPM: minor updates.
- Add Requires iptables (duh)

* Tue Jun 19 2012 Casper Gielen <cgielen@uvt.nl>
- replace dependency on redhat-lsb-core with dependency on redhat-lsb: fix for
  RHEL.

* Tue Jun 12 2012 Casper Gielen <cgielen@uvt.nl>
- Add dependency on redhat-lsb-core: fix for Fedora >= 12.

* Tue Aug 31 2011 Joost van Baal <joostvb@uvt.nl> 20110831-1
- New upstream.  (Missed 20100717, 20100820, 20100821, 20100823, 20100831,
  20110213, 20110602, 20110608.)
- Add BuildArch: noarch, since we _are_ noarch.

* Mon May 26 2008 Joost van Baal <joostvb@uvt.nl> 20080330-1
- New upstream.

* Tue Jan 22 2008 Joost van Baal <joostvb@uvt.nl> 20071101-1
- New upstream (missed 20071030)
- NB: uruk 20051129-1 did NOT solve your troubles when you've merely upgraded
  from < 20051129-1.  You'll have to uninstall your uruk before installing uruk
  >= 20051129-1.  For now, this is the only way to get fixed rc.d symlinks to
  /etc/init.d/uruk...

* Wed Nov 30 2005 Joost van Baal <joostvb@uvt.nl> 20051129-1
- New upstream.
- Install uruk-save.* in doc/.
- s/Copyright/License/: new RPM format.

* Tue Nov 15 2005 Joost van Baal <joostvb@uvt.nl> 20051027-1
- New upstream (missed 20050414, 20050718)
- README.RPM: added hint on how to disable RH-style fw rules
- Now maintained at http://arch.gna.org/uruk/
- Tweaked Summary and description (as in Debian package)
- uruk-source.1.README.RPM: added note on chkconfig(8)
- Added uruk-save binary and manpage.

* Fri Jul 30 2004 Joost van Baal <joostvb@uvt.nl> 20040625-1
- New upstream

* Mon Feb 16 2004 Joost van Baal <joostvb@uvt.nl> 20040216-1
- New upstream.  (Old one was _severely_ broken!)
- README.RPM: refer to sample rc file.

* Mon Feb 16 2004 Joost van Baal <joostvb@uvt.nl> 20040213-1
- Initial specfile.  Untested!

