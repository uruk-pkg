debian.mbox:
	querybts --mbox --source uruk >$@

mutt: debian.mbox
	mutt -R -f $<

.PHONY: mutt
