uruk (20240930-1) unstable; urgency=medium

  * New upstream release: The Surae Release.
  * d/control: add Suggests: jq.  Only needed by usr/lib/uruk/docker-netns
    as called by u/s/doc/uruk/examples/docker-netns.service .  Thanks Sander
    van Dinten for catching this one.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 30 Sep 2024 13:29:38 +0200

uruk (20240902-1) unstable; urgency=medium

  * New upstream release: The Baardwijkse Overlaat Release.
    - init/autodetect-ips: deal a bit more sane with nics with :, - or @ in
      their name.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 03 Sep 2024 07:37:45 +0200

uruk (20240719-1) unstable; urgency=medium

  * New upstream release: The Brobbelbies Release.
    - Fix "Fails to build binary packages again after successful build": This
      now finally really Closes: #1049742
    - Now ships Wessel Danker's /usr/lib/uruk/docker-netns, see
      /u/s/d/uruk/docker-and-iptables.txt
      + debian/copyright: add WTFPL-2.

  [ Helmut Grohne ]
  * debian/rules: move files to /usr for DEP17. (Closes: #1063692)

  [ Joost van Baal-Ilić ]
  * debian/rules: fix: chown: warning: '.' should be ':': „root.root“
  * debian/control: add explicit Rules-Requires-Root: binary-targets (thanks
    lintian)
  * debian/control: fix extended description: code size of main uruk script
    grew from 14 K to 17 K.
  * debian/TODO: remove some obsolete stuff about rumoured bugs from the
    Debian jessie w/ uruk 20160219-1 era.
  * debian/lintian-overrides: suppress "troff:<standard input>:...:
    warning: cannot select font 'C'": probably caused by reported
    Bug #1076648 in the aephea typesetting system.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sat, 20 Jul 2024 13:43:28 +0200

uruk (20231009-1) unstable; urgency=medium

  * New upstream release: The Vyttila - Kakkanad Release.
    - should partly address Debian Bug #925297 "uruk: migrate from iptables
      and ip6tables to nftables"
    - closes Bug "uruk: ip[6]tables has been moved to /usr/sbin/ip[6]tables"
      (thanks Casper Gielen).  Closes: #951049
  * debian/rules: deal with "uruk: Fails to build binary packages again after
    successful build": do not call 'make distclean' but 'make clean' in the
    debian/rules clean target.  Thanks Lucas Nussbaum.  Closes: #1049742
  * debian/lintian-overrides: explicitly specify "binary: " in order to make
    current lintian happy.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 09 Oct 2023 14:30:23 +0200

uruk (20190121-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:55:02 +0200

uruk (20190121-2) unstable; urgency=low

  [ https://lintian.debian.org/sources/uruk ]
  * debian/source/lintian-overrides: added: get rid of "I
    debian-rules-parses-dpkg-parsechangelog (line 19)".
  * debian/NEWS: get rid of asterisks cf § 6.3.5. "Supplementing changelogs
    with NEWS.Debian files" in Debian Developer's Reference.

  [ lintian-brush ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 26 Dec 2021 09:44:28 +0100

uruk (20190121-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 16:34:46 +0100

uruk (20190121-1) unstable; urgency=medium

  * New upstream release: The van Keistoep naar Heisteeg Release.
  * debian/rules: generate DEBIAN/md5sums in a reproducible way, see issue
    "random_order_in_md5sums" in https://reproducible-builds.org/ 's Debian
    tests.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 21 Jan 2019 10:11:30 +0100

uruk (20181116-1) unstable; urgency=medium

  * New upstream release: The Lombardijen Release (missed 20181113 - The Alfama
    Release, 20181112 - The Plantagedok Release, 20181109 - The Catharijnepoort
    Release, 20181107 - The Daničareva Release)
    - In case runtime configuration of IPs to network interfaces is available
      and differs from what's specified in /etc/network/interfaces, use the
      runtime information.  Therefore, you might now observe different uruk
      behaviour if you use e.g. udev to assign IPs to your network interfaces.
  * debian/rules: Call dpkg-deb with -Zgzip.  This generates a .deb which is
    installable on Debian 7/wheezy (and later releases too, of course).  If
    you'd like to build with current dpkg-deb's default, set
    DPKG_DEB_OPTS='-Zxz'.  See debian/rules for more information.
  * debian/postinst: do not print an error message on systems lacking
    deb-systemd-helper (e.g. systems running Debian 7/wheezy and older): we
    assume we don't need to run it if it's not installed.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 16 Nov 2018 14:06:14 +0100

uruk (20181006-1) unstable; urgency=medium

  * New upstream release: The Tidal Basin, Washington, D.C. Release

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 26 Oct 2018 15:12:45 +0200

uruk (20181005-1~uvt1) unstable; urgency=medium

  * New upstream release: The Dampad Release
    - We now start more reliably during boot.  See /usr/share/doc/uruk/NEWS
      for details.
  * debian/postrm: add deb-systemd-helper mask|unmask|purge when
    appropriate in order to properly deregister systemd unit on removal.
    Thanks Andreas Beckmann for bugreport. Closes: #909603

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 05 Oct 2018 10:49:59 +0200

uruk (20180528-2) unstable; urgency=medium

  * debian/postinst: call "deb-systemd-helper enable uruk" if needed.
    Thanks Lars Biemans and Wessel Dankers for reporting and suggesting a
    solution (via IRC).
  * debian/TODO: more notes on Ubuntu's ufw, more to do for debian/watch.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 30 Aug 2018 16:36:01 +0200

uruk (20180528-1) unstable; urgency=medium

  * New upstream release: The Verliefd Laantje Release
  * debian/control: Standards-Version compliancy upgraded from 3.9.7
    (currently obsolete) to 3.9.8 (no further changes needed).

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 28 May 2018 14:04:53 +0200

uruk (20160219-2) unstable; urgency=medium

  * debian/copyright: move section 'Files: *' even more up, pleasing lintian.
  * debian/{ifup,rules,conffiles}: ifup removed: no longer install
    /etc/network/if-up.d/uruk .  It interacts _very_ badly with systems using
    the systemd system and service manager.  Thanks Wessel Dankers and Casper
    Gielen for help in debugging and testing this.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 25 May 2018 13:50:46 +0200

uruk (20160219-1) unstable; urgency=medium

  * New upstream release: The Speurgt Release
  * debian/NEWS: Add item on enabling uruk-save by default.
  * debian/control: Standards-Version compliancy upgraded from 3.9.6 to 3.9.7
    (no further changes needed).

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 19 Feb 2016 11:16:04 +0100

uruk (20160218-1) unstable; urgency=low

  * New upstream release: The Snijders-Chaam Release
  * debian/rules: do install upstream /lib/systemd/system/uruk.service.
    See also Bug#796700 "uruk: Has init script in runlevel S but no matching
    service file" by Felipe Sateler and Andreas Henriksson.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 18 Feb 2016 15:15:04 +0100

uruk (20150921-1) unstable; urgency=low

  * New upstream release: The Prishtinë Release
    - debian/rules: do not install upstream uruk.service in /lib/systemd/system,
      but, as long as this is highly experimental code, in /usr/share/doc/uruk.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 21 Sep 2015 12:57:12 +0200

uruk (20150916-1) unstable; urgency=low

  * New upstream release: The ᎠᏍᎦᏯ ᎩᎦᎨᏱ; Release

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 16 Sep 2015 10:30:36 +0200

uruk (20150825-1) unstable; urgency=low

  * New upstream release: The Прибој Release.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 25 Aug 2015 13:35:35 +0200

uruk (20150810-1) unstable; urgency=low

  * New upstream release: The Гoрњи Милановац Release (missed uruk version
    20150608 - The Oude Leije Release).
  * debian/rules: explicitly set timestamps on all files installed by package.
    Now uruk can (really!) be built reproducibly in the current experimental
    Debian ReproducibleBuilds framework.  Thanks Maria "akira" Valentina Marin
    and Jérémy "lunar" Bobbio e.a. for the patch.  Closes: #792986
  * debian/rules: honor DPKG_DEB_OPTS for specific builds.  We now can "easily"
    build a .deb for Debian lenny, which does not support xz-compressed .deb's.
    Thanks Casper Gielen for bugreport.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 10 Aug 2015 10:54:58 +0200

uruk (20150401-1) unstable; urgency=low

  * New upstream release: The Gorp en Roovert Release (missed: uruk version
    20150325 - The De Drie Zwaantjes Release).
    - The De Drie Zwaantjes Release makes it easier to configure uruk; new
      syntax is supported (old configurations for now keep being fully
      supported too).  See the upstream uruk NEWS file (in
      /usr/share/doc/uruk/NEWS or online at
      http://mdcc.cx/pub/uruk/uruk-latest/NEWS) for details.
    - The De Drie Zwaantjes Release introduced a showstopper bug which was fixed
      in The Gorp en Roovert Release ("drop all traffic when multiple addresses
      are used")
    - Uruk upstream development is no longer a one man show: Since this release,
      Wessel Dankers joined the uruk maintainers. Welcome aboard!
  * debian/rules: remove timestamps from the build system. Now uruk can be built
    reproducibly in the current experimental Debian ReproducibleBuilds
    framework.  Thanks Chris Lamb for the bugreport and patch.  Closes: #776968

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 12 May 2015 12:52:32 +0200

uruk (20141120-1) experimental; urgency=low

  * New upstream release: The Јадар Release.
  * debian/copyright: clarify license on doc/rfc4890-icmpv6-firewall.sh: add
    short license field.  Thanks lintian.
  * debian/control: Standards-Version compliancy upgraded from 3.9.5 to 3.9.6
    (no further changes needed).
  * debian/{rules,lintian-overrides}: added lintian overrides.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 25 Nov 2014 15:52:27 +0100

uruk (20140627-1) unstable; urgency=low

  * New upstream release: The Vlook Release.
  * debian/control: add Vcs-Browser field.  No interactive browsing, yet.
  * debian/control: Standards-Version compliancy upgraded from 3.9.3 to 3.9.5
    (no further changes needed).

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 08 Jul 2014 18:03:58 +0200

uruk (20140319-1) unstable; urgency=low

  * New upstream release: The Alfama Release.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 20 Mar 2014 16:59:55 +0100

uruk (20131213-1) unstable; urgency=low

  * New upstream release: The Gweek Release (Missed 20130913 - The Clochán na
    bhFomhórach Release, 20130830 - The Dr Syntax's Head Release.)
    - script/urukctl: fix severe bug, urukctl was unusable.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 13 Dec 2013 16:26:28 +0100

uruk (20130809-1) unstable; urgency=low

  * New upstream release: The Corbeşti Release.  (Missed 20130619 The Het De
    Siptenpad Release.)
    - init/autodetect-ips: detect IPs currently assigned to interfaces by
      calling ip(8) if needed. Closes: #712869
  * copyright: updated.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 02 Aug 2013 08:14:42 +0200

uruk (20130618-1) unstable; urgency=low

  * This package is not publicly released.
  * New upstream release: The Sterreke Release.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 18 Jun 2013 09:35:49 +0200

uruk (20130426-1) unstable; urgency=low

  * New upstream release: The Sy Release - 10th anniversary release
    - Replace obsolete 'state' module usage with 'conntrack'. Closes: #702064
    - Fixed typo on IPv6 reserved IP addresses. Closes: #705202
  * control: fix typo in Vcs-Git. Thanks Thijs Kinkhorst.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sat, 13 Apr 2013 16:11:41 +0200

uruk (20130226-1) unstable; urgency=low

  * New upstream release: The Vlist Release.
  * debian/control: Add "Conflicts: ufw", since ufw is installed by default on
    derivative distribution Ubuntu.
  * debian/copyright: finally convert to copyright-format 1.0.  Files lsb/lsb_*,
    lsb/init-functions no longer listed as "GPL" but as "GPL-2+".

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 26 Feb 2013 11:16:55 +0100

uruk (20121205-1) unstable; urgency=low

  * New upstream release: The Zes Blokskes Release.
  * debian/copyright: do not refer to unversioned symlink of GPL. tnx lintian.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 06 Dec 2012 09:23:54 +0100

uruk (20121130-1) unstable; urgency=low

  * New experimental upstream: The Вршац Release.
  * This upstream release behaves slightly different: in case you don't have any
    IPv6-configuration in /etc/network/interfaces, uruk will no longer call
    ip6tables: this release ships a tweaked /etc/default/uruk, using
    /lib/uruk/init/enable-ipv6.
  * debian/rules: make sure new scripts
    /libexec/uruk/init/{autodetect-ips,enable-ipv6} end up in
    /lib/uruk/init/.
  * debian/copyright: add license information for
    init/{autodetect-ips,enable-ipv6}.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 04 Dec 2012 19:18:58 +0100

uruk (20121023-1) unstable; urgency=low

  * New upstream release: The Grafwegen Release.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 23 Oct 2012 13:58:21 +0200

uruk (20121005-1) unstable; urgency=low

  * New upstream release: The Onze-Lieve-Vrouw-Waver Release.
  * debian/copyright: Add copyright information for new upstream files in lsb/.
  * debian/rules: do not ship lsb/ files in binary package: needed on Red Hat
    only.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 19 Oct 2012 15:30:08 +0200

uruk (20120914-1) unstable; urgency=low

  * New upstream release: The Sankt Goar Release.
    - No longer incorrectly block final FIN-ACK packets.  Closes: #687621
    - Again some fiddling with init script (this Debian package suffers from a
      bug reported by lintian as: "E: uruk:
      init.d-script-missing-dependency-on-local_fs etc/init.d/uruk:
      required-stop".  That bug has been fixed upstream by now.)
  * debian/watch: make sure it no longer matches the uruk-latest.tar.gz symlink.
  * debian/dirs: removed, no longer need to create /usr/sbin.
  * debian/rules: no longer use dirs.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 05 Oct 2012 15:05:02 +0200

uruk (20120608-1) unstable; urgency=low

  * New upstream release: The Hooidonk Release.
    - updated lsb headers in init script
  * debian/watch: added: uscan support.
  * debian/rules: install scripts in /sbin, not /usr/sbin.  Early in boot, /usr
    might not yet have been mounted.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 12 Jun 2012 15:47:10 +0200

uruk (20120605-1) unstable; urgency=low

  * New upstream release:
    - no longer die if programs zoem, col and/or groff are not found.  (Thanks
      Lucas Nussbaum for reporting the issue.)  Closes: #676067

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 05 Jun 2012 07:50:23 +0200

uruk (20120530-1) unstable; urgency=low

  * New upstream release.
  * debian/{ifup,rules,conffiles}: ifup added, installed as
    /etc/network/if-up.d/uruk.  We now support ifup/ifdown and networkmanager.
  * debian/control: no longer Build-Depends-Indep upon zoem (>= 10-265), aephea,
    groff-base, bsdmainutils: upstream now ships typesetted docs.
  * debian/copyright: document license of new upstream file
    doc/rfc4890-icmpv6-firewall.sh
  * debian/source/format: changed to 3.0 (quilt) format.
  * debian/rules: add recommended targets build-arch and build-indep.
  * debian/control: Standards-Version compliancy upgraded from 3.9.2 to 3.9.3
    (no further changes needed).
  * debian/control: changed my name.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 30 May 2012 16:31:54 +0200

uruk (20110608-1) unstable; urgency=low

  * New upstream release: The IPv6 Day release!.  Missed 20110213 and
    20110602.
  * debian/control: Standards-Version compliancy upgraded from 3.9.1 to
    3.9.2 (no further changes needed).
  * control: build-depends: add aephea, update zoem (>= 10-265):
    incompatible zoem changes.

 -- Joost van Baal <joostvb@debian.org>  Wed, 08 Jun 2011 08:28:46 +0200

uruk (20100831-2) unstable; urgency=low

  * postinst: add "1" to the runlevels to stop the service in update-rc.d
    call: back in sync with LSB headers in upstream supplied init-script
    (Closes: #581659).

 -- Joost van Baal <joostvb@debian.org>  Tue, 31 Aug 2010 14:26:45 +0200

uruk (20100831-1) unstable; urgency=low

  * New upstream release.

 -- Joost van Baal <joostvb@debian.org>  Tue, 31 Aug 2010 12:31:45 +0200

uruk (20100823-1) unstable; urgency=low

  * New upstream release.  Missed 20100820 (and 20100821), which introduced:
    + Fix bugs in support for dependency based boot sequencing (which is the
      default in "Squeeze"; Boot Performance is a goal for the upcoming Debian
      "Squeeze" release)
      - init/uruk.in: Fix LSB init header: we want to start early in boot
        sequence (on entering runlevel S).  LSB init.d header however had
        "Default-Start: 2 3 5". This is now fixed to S.  Partially fixes
        bug 581659. (Upgrade's are likely not yet handled gracefully.) Thanks
        Petter Reinholdtsen for reporting and for the patch.
      - Furthermore, change Default-Stop: "0 6" to "0 1 6": no need to special
        case runlevel 1 (thanks lintian).
      - Finally, added "$remote_fs" to Required-Start: and Required-Stop: since
        obviously we need /usr/sbin/uruk to be available (thanks again lintian).
    + Enable support for IPv6 packet filtering (full IPv6 support
      is a goal for the upcoming Debian "Squeeze" release).  ip6tables is now
      enabled in the uruk script by default.  If you interact with uruk using
      the init script (which is the default), uruk's default behaviour however
      is not changed.
  * debian/copyright: updated to new upstream.
  * debian/control: the uruk script size no longer is 11 K but 13 K: update
    description.
  * debian/control: Standards-Version compliancy upgraded from 3.8.4 to
    3.9.1 (no further changes needed).

 -- Joost van Baal <joostvb@debian.org>  Mon, 23 Aug 2010 10:32:51 +0200

uruk (20100717-1) unstable; urgency=low

  * This package is not publicly released.
  * New upstream release.
  * debian/control: Standards-Version compliancy upgraded from 3.7.3 to 3.8.4.
  * debian/control: This code no longer is maintained using GNU Arch but
    using the git Version Control System
  * debian/NEWS: fix typo. Thanks lintian.

 -- Joost van Baal <joostvb@debian.org>  Sun, 18 Jul 2010 08:25:55 +0200

uruk (20080330-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Add Vcs-Arch header.  Unfortunately, gna.org does not
    offer archzoom, so no Vcs-Browser header for now.
  * debian/copyright: machine-interpretable, using proposal by Sam Hocevar
    (http://wiki.debian.org/Proposals/CopyrightFormat).

 -- Joost van Baal <joostvb@debian.org>  Sun, 30 Mar 2008 15:21:18 +0200

uruk (20080307-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Don't use unneeded Debian revisions in build dependencies:
    be nice to backporters.  (Thanks lintian.)

 -- Joost van Baal <joostvb@debian.org>  Sat, 08 Mar 2008 08:44:32 +0100

uruk (20071101-3) unstable; urgency=low

  * debian/postinst: enable upgrading a not yet configured uruk.  Thanks
    Wessel Dankers for reporting this bug (in private communication).
  * debian/control: move homepage to proper field.
  * debian/control: Standards-Version compliancy upgraded from 3.7.2 to
    3.7.3 (no further changes needed).

 -- Joost van Baal <joostvb@debian.org>  Thu, 07 Feb 2008 15:54:17 +0100

uruk (20071101-2) unstable; urgency=low

  * Ignore errors from init script when removing package: these might be
    caused by lacking (manual) configuration of the package.  Thanks
    Jacob Holst (Closes: #452904).

 -- Joost van Baal <joostvb@debian.org>  Tue, 04 Dec 2007 16:39:45 +0100

uruk (20071101-1) unstable; urgency=low

  * New upstream release. Missed 20071030, which introduced:
    - no longer try to support non-ascii characters in .txt manpages
      (Closes: #441659)
    - If you're using hook scripts (by setting $rc_a, $rc_b, ... or $rc_i
      in your uruk rc file), you might be hit by an incompatibility.  See
      upstream NEWS file (in /usr/share/doc/uruk/NEWS) for details.
    - The uruk init script now is (should be) Linux Standards Base v 3.1.0
      compliant.
    See upstream NEWS file for other changes and new features in the 20071030
    and 20071101 releases.
  * debian/copyright: no longer GPL v2, but v3 (or higher).  Add MIT
    license for Fred Vos's XML stuff in contrib/.
  * debian/rules: do not ignore error from "make distclean".  (Thanks
    lintian).
  * debian/rules: compress NEWS.Debian (Thanks lintian).
  * debian/control: Standards-Version compliancy upgraded from 3.6.2.0 to
    3.7.2 (no further changes needed).

 -- Joost van Baal <joostvb@debian.org>  Sun, 04 Nov 2007 15:55:38 +0100

uruk (20051129-1) unstable; urgency=low

  * New upstream release.
  * control: build-depend upon zoem >= 05-328: new \tr semantics
    (Closes: #354677).
  * control: found out how to add Homepage pseudo-field (finally!).

 -- Joost van Baal <joostvb@debian.org>  Thu, 02 Mar 2006 11:13:55 +0100

uruk (20051027-1) unstable; urgency=low

  * New upstream release.  (20051026 suffered from an annoying bug, no public
    Debian package for that one was released.)
  * debian/rules: added NEWS.Debian for documenting incompatibly changes.
  * debian/{prerm,postinst}: make sure firewall rules are no longer disabled
    during upgrade.  They now are merely reloaded:
    + prerm: call "invoke-rc.d uruk stop" only on package removal.
    + postinst: call "invoke-rc.d uruk force-reload" on upgrade.
  * debian/control: changed package description.
  * /etc/uruk/rc, README.Debian: add some pointers to quick setup guide in
    uruk(8).
  * debian/{rules,uruk.default}: default script now shipped upstream, we
    install that one.
  * debian/rules: don't install .ps and .txt copies of manpages: these are
    easily generated from other installed formats.
  * debian/rules: generate md5sums (implementation inspired by dh_md5sums).
  * debian/control: s/joostvb-uruk@mdcc.cx/joostvb@debian.org/
  * debian/control: Standards-Version compliancy upgraded from 3.6.1.0 to
    3.6.2.0 (no further changes needed).
  * First upload to the Debian archive (Closes: #332819)

 -- Joost van Baal <joostvb@debian.org>  Sun, 30 Oct 2005 18:44:05 +0100

uruk (20050718-1) unstable; urgency=low

  * New upstream pre-release.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Mon, 18 Jul 2005 18:04:50 +0200

uruk (20050414-1) unstable; urgency=low

  * New upstream prerelease.
  * conffiles: add /etc/default/uruk, used by init script.
  * control: make homepage stand out.
  * This package now is maintained at http://arch.gna.org/uruk/ .
  * rules, uruk.default: ship example /etc/default/uruk script.
  * rules: compress new ChangeLog.2003 file.
  * control: checked standards compliance: bumped from 3.5.8 to 3.6.1.0.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Fri, 15 Apr 2005 23:24:37 +0200

uruk (20040625-1) unstable; urgency=low

  * New upstream.  BEWARE!  This is a prerelease.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Fri, 25 Jun 2004 10:43:34 +0200

uruk (20040216-1) unstable; urgency=low

  * New upstream.  BEWARE!  Upstream 20040213 was _severely_ broken.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Mon, 16 Feb 2004 16:25:07 +0100

uruk (20040213-1) unstable; urgency=low

  * New upstream.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Fri, 13 Feb 2004 17:05:03 +0100

uruk (20040210-1) unstable; urgency=low

  * New upstream.  Beware: slightly changed defaults in ICMP blocking.
    See upstream NEWS file.
  * rc: point to the right places to get uruk initialized and configured.
  * postinst: instead of update-rc.d's default we now use "start 40 S . stop
    89 0 6 ." for the uruk runlevels (the same as used by other iptables
    calling packages).

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Tue, 10 Feb 2004 15:03:09 +0100

uruk (20031111-1) unstable; urgency=low

  * New upstream.  Beware: backwards incompatibility.  See upstream docs.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Tue, 11 Nov 2003 13:44:47 +0100

uruk (20031026-1) unstable; urgency=low

  * New upstream (missed 20031008).
  * First public release of this Debian package.
  * README.Debian, TODO.Debian added.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Sun, 26 Oct 2003 16:49:50 +0100

uruk (20031005-1) unstable; urgency=low

  * New upstream.  (Not publicly released.)
  * rules: copyleft statement added.
  * control: description spiced up.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Sun, 05 Oct 2003 20:38:10 +0200

uruk (20031004-1) unstable; urgency=low

  * Initial release.

 -- Joost van Baal <joostvb-uruk@mdcc.cx>  Sun, 05 Oct 2003 14:40:32 +0200
